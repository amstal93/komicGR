import * as mysql from 'promise-mysql';

const poolPromise = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME
});

class UnitOfWorkFactory {
  static async createPool(): Promise<mysql.Pool> {
    const pool = await poolPromise;

    return pool;
  }
}

export default UnitOfWorkFactory;
