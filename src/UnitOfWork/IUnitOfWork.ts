import { QueryOptions, Query } from "mysql";

// TODO: Fix these types
interface IUnitOfWork {
    beginTransaction();
    query(sql: string | Query | QueryOptions);
    rollback();
    commit();
    release();
    escape(input: any);
}

export default IUnitOfWork;