import { injectable, inject } from "inversify";

import ICategoryService from "./ICategoryService";
import ICategoryRepository from "../../DataAccess/Sql/Repositories/Category/ICategoryRepository";

import AddCategoryDTO from "../../Domain/Dtos/Category/AddCategoryDTO";
import Category from "../../Domain/Models/Category/Category";
import TYPES from "../../Helpers/DI/Types";

@injectable()
class CategoryService implements ICategoryService {

    constructor(@inject(TYPES.ICategoryRepository) private categoryRepository: ICategoryRepository) {}

    addCategory(newCategory: AddCategoryDTO): Promise<any> {
        return this.categoryRepository.addCategory(newCategory);
    }

    setPostCategory(postId: number, categoryId: number): Promise<any> {
        return this.categoryRepository.setPostCategory(postId, categoryId);
    }

    getPostCategories(): Promise<Category[]> {
        return this.categoryRepository.getPostCategories();
    }

}

export default CategoryService;