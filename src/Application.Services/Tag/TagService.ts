import { injectable, inject } from "inversify";
import ITagService from "./ITagService";
import ITagRepository from "../../DataAccess/Sql/Repositories/Tag/ITagRepository";
import Tag from "../../Domain/Models/Tag/Tag";
import TYPES from "../../Helpers/DI/Types";

@injectable()
class TagService implements ITagService {
  constructor(
    @inject(TYPES.ITagRepository) private tagRepository: ITagRepository
  ) {}

  getTags(): Promise<Tag[]> {
    return this.tagRepository.getTags();
  }
}

export default TagService;
