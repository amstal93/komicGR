import { Request, Response } from "express";
import * as sinon from "sinon";
import * as chai from "chai";
const expect = chai.expect;

import { handleGetTags } from "../../../WebApi/Controllers/TagsController";
import ITagService from "../../../Application.Services/Tag/ITagService";
import { TagServiceMOCK } from "../../Mocks/TagServiceMock";

describe("GetTagsRequestHandler", () => {
  let request: Request;
  let response: Response;
  let tagService: ITagService;

  beforeEach(() => {
    tagService = new TagServiceMOCK();
  });

  it("should return all the tags", async () => {
    const mockedTags = [
      { id: 1, name: "tag1" },
      { id: 2, name: "tag2" },
    ];
    const stubGetTags = sinon
      .stub(tagService, "getTags")
      .callsFake(() => Promise.resolve(mockedTags));
    const handler = handleGetTags(request as Request, response as Response, tagService);
    const result = await handler;

    stubGetTags.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result).equal(mockedTags);
  });
});
