import { Request, Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleUploadProfileImageUrlRequest } from '../../../WebApi/Controllers/UserController/index';
import IUserService from "../../../Application.Services/User/IUserService";
import { UserServiceMOCK } from "../../Mocks/UserServiceMock";
import User from '../../../Domain/Models/User/User';

describe('UploadProfileImageUrlHandler', () => {
    let userServiceMock: IUserService;
    let response: Partial<Response>;
    let request: Partial<Request>;

    beforeEach(() => {
        request = {
            body: {
                username: 'username',
                password: 'password'
            },
            params: {},
            userFromPassport: {
                id: 1
            } as User
        };
        userServiceMock = new UserServiceMOCK();
    });

    it('should return an object with success property true and msg "Profile Image Url added succesfully"', async () => {
        const stubSetProfileImageUrl = sinon.stub(userServiceMock, "setProfileImageUrl").callsFake(() => Promise.resolve(true));

        const handler = handleUploadProfileImageUrlRequest(request as Request, response as Response, userServiceMock);
        const updated = await handler;

        stubSetProfileImageUrl.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(updated).to.deep.equal({ success: true, msg: "Profile Image Url added succesfully" });
    });

    it('should return an object with success property fale and msg "Profile Image Url added succesfully"', async () => {
        const stubSetProfileImageUrl = sinon.stub(userServiceMock, "setProfileImageUrl").callsFake(() => Promise.resolve(false));

        const handler = handleUploadProfileImageUrlRequest(request as Request, response as Response, userServiceMock);
        const updated = await handler;

        stubSetProfileImageUrl.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(updated).to.deep.equal({ success: false, msg: "Profile Image Url added succesfully" });
    });
})