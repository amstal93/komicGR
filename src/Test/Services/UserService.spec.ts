import { injectable, Container } from "inversify";
import TYPES from "../../Helpers/DI/Types";

import sinon from 'sinon';
import chai from 'chai';
import { Request, Response } from 'express';
const expect = chai.expect;

import UserService from '../../Application.Services/User/UserService';
import IUserRepository from "../../DataAccess/Sql/Repositories/User/IUserRepository";

import User from "../../Domain/Models/User/User";
import Role from "../../Domain/Models/Role/Role";
import { QueryResponse } from "../../Domain/Dtos/QueryResponse";
import RegisterUserDTO from "../../Domain/Dtos/User/RegisterUserDTO";
import BcryptService from "../../Helpers/Bcryptjs/BcryptService";

function createTestUserWithId(id: number): User {
    return new User(
        id,
        Role.Human,
        "Test First Name",
        "Test Last Name",
        "test" + id + "@test.com",
        "test_username_" + id,
        "test_pass",
        "test job_desc",
        "test address",
        11111111,
        "http://www.someurl.com",
        new Date()
    )
}

function createRegisterUserDTO(): RegisterUserDTO {
    return {
        first_name: "first_name",
        last_name: "last_name",
        email: "test@test.com",
        username: "test username",
        password: "test pass",
        job_desc: "test job_desc",
        address: "test address",
        mobile: 111111111
    };
}

@injectable()
class UserRepositoryMock implements IUserRepository {
    getAll(): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getById(id: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getByUsername(username: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getByEmail(email: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getUserByPostId(postId: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    insertUser(user: import("../../Domain/Dtos/User/RegisterUserDTO").default): Promise<any> {
        throw new Error("Method not implemented.");
    }
    setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    setProfileImageUrl(userId: number, profileImageUrl: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    updatePassword(userId: number, newPassword: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}

describe("User Service", () => {
    let userService: UserService;
    let userRepositoryMock: IUserRepository;
    let bcryptService: BcryptService;
    let container: Container;
    const testUserId1 = 1;
    let user1: User;
    let request: Request;

    beforeEach(() => {
        container = new Container();
        container.bind<IUserRepository>(TYPES.IUserRepository).to(UserRepositoryMock);
        container.bind<BcryptService>(TYPES.BcryptService).to(BcryptService);
        userRepositoryMock = container.get(TYPES.IUserRepository);
        bcryptService = container.get(TYPES.BcryptService);
        userService = new UserService(userRepositoryMock, bcryptService);
        user1 = createTestUserWithId(testUserId1);
    });

    it("should return a Promise with an array of users, when calling getAll", async () => {
        const testUserId2 = 2;
        const user2 = createTestUserWithId(testUserId2);
        const testUsers = [user1, user2];

        const stubHandler = sinon.stub(userRepositoryMock, "getAll").callsFake(() => {
            return Promise.resolve(testUsers);
        });

        const users: User[] = await userService.getAll();
        stubHandler.restore();

        expect(stubHandler.calledOnce).to.be.true;
        expect(users).to.deep.equal(testUsers);
        expect(users.length).to.equal(testUsers.length);
        expect(users[0]).to.be.instanceOf(User);
        // Maybe test if then and catch are functions to test if it is actually a promise
    });

    it("should return a Promise with an array of users, when calling getById", async () => {
        const stubHandler = sinon.stub(userRepositoryMock, "getById").callsFake(() => {
            return Promise.resolve(user1);
        });

        const user: User = await userService.getById(user1.id);

        stubHandler.restore();
        expect(user).to.deep.equal(user1)
        expect(stubHandler.calledOnceWith(testUserId1)).to.be.true;
    });

    it("should return a Promise with a user, when calling getUserByPostId", async () => {
        const demoPostId = 1;

        const stubHandler = sinon.stub(userRepositoryMock, "getUserByPostId").callsFake(() => {
            return Promise.resolve(user1);
        });

        const user = await userService.getUserByPostId(demoPostId);

        stubHandler.restore();
        expect(user).to.deep.equal(user1);
        expect(stubHandler.calledOnceWith(demoPostId)).to.be.true;
    });

    it("should return a Promise with a user, when calling getByUsername", async () => {
        const stubHandler = sinon.stub(userRepositoryMock, "getByUsername").callsFake(() => {
            return Promise.resolve(user1);
        });

        const user = await userService.getByUsername(user1.username);

        stubHandler.restore();
        expect(user).to.deep.equal(user1);
        expect(stubHandler.calledOnceWith(user1.username)).to.be.true;
    });

    it("should return a Promise with a user, when calling getByEmail", async () => {
        const stubHandler = sinon.stub(userRepositoryMock, "getByEmail").callsFake(() => {
            return Promise.resolve(user1);
        });

        const user = await userService.getByEmail(user1.email);

        stubHandler.restore();
        expect(user).to.deep.equal(user1);
        expect(stubHandler.calledOnceWith(user1.email)).to.be.true;
    });

    it('should return a Promise with a boolean, when calling setConfirmedEMail', async () => {
        const stubHandler = sinon.stub(userRepositoryMock, "setConfirmedEmail").callsFake(() => {
            return Promise.resolve(true);
        });

        const confirmed = await userService.setConfirmedEmail(user1.id, true);

        stubHandler.restore();
        expect(confirmed).to.be.true;
        expect(stubHandler.calledOnceWith(user1.id, true)).to.be.true;
    })

    it("should return a Promise with a boolean, correctly when calling setImageUrl", async () => {
        const profileImageUrl = "http://www.someurl.com";

        const stubHandler = sinon.stub(userRepositoryMock, "setProfileImageUrl").callsFake(() => {
            return Promise.resolve(true);
        });

        const updated = await userService.setProfileImageUrl(user1.id, profileImageUrl);

        stubHandler.restore();
        expect(updated).to.be.true;
        expect(stubHandler.calledOnceWith(user1.id, profileImageUrl)).to.be.true;
    });

    it("should return a Promise with a boolean, correctly when calling updatePassword", async () => {
        const newPassword = "newPassword"

        const stubHandler = sinon.stub(userRepositoryMock, "updatePassword").callsFake(() => {
            return Promise.resolve(true);
        });

        const updated = await userService.updatePassword(user1.id, newPassword);

        stubHandler.restore();
        expect(updated).to.be.true;
        expect(stubHandler.calledOnceWith(user1.id, newPassword)).to.be.true;
    });

    it("should return a Promise with a QueryResponse, correctly when calling inserUser", async () => {
        const registerUserDto = createRegisterUserDTO();
        const notHashedPassword = registerUserDto.password;
        const queryResponse: QueryResponse = {
            fieldCount: 0,
            affectedRows: 1,
            insertId: 1,
            serverStatus: 0,
            warningCount: 0,
            message: '',
            protocol41: true,
            changedRows: 1
        }
        const salt = 'salt';
        const hashkey = 'hashkey';

        const stubHandler = sinon.stub(userRepositoryMock, "insertUser").callsFake(() => {
            return Promise.resolve(queryResponse);
        });
        const stubSalt = sinon.stub(BcryptService, "generateSalt").returns(Promise.resolve(salt));
        const stubHash = sinon.stub(BcryptService, "createHashKey").returns(Promise.resolve(hashkey));

        const inserted = await userService.insertUser(registerUserDto);

        stubHandler.restore();
        stubSalt.restore();
        stubHash.restore();
        expect(inserted.insertId).to.equal(queryResponse.insertId);
        expect(stubSalt.calledOnceWith(10)).to.be.true;
        expect(stubHash.calledAfter(stubSalt)).to.be.true;
        expect(stubHash.calledOnceWith(notHashedPassword, salt)).to.be.true;
        expect(registerUserDto.password).to.not.equal(notHashedPassword);
    });

    it("should return a Promise with a value of true, when calling comparePassword", async () => {
        const password = "password";
        const hashKey = "hashKey";

        const stubCompare = sinon.stub(BcryptService, "comparePassword").returns(Promise.resolve(true));

        const isMatch = await userService.comparePassword(password, hashKey);

        stubCompare.restore();
        expect(stubCompare.calledOnceWith(password, hashKey)).to.be.true;
        expect(isMatch).to.equal(true);
    });

    it("should return a Promise with a value of false, when calling comparePassword", async () => {
        const password = "password";
        const hashKey = "hashKey";

        const stubCompare = sinon.stub(BcryptService, "comparePassword").returns(Promise.resolve(false));

        const isMatch = await userService.comparePassword(password, hashKey);

        stubCompare.restore();
        expect(stubCompare.calledOnceWith(password, hashKey)).to.be.true;
        expect(isMatch).to.equal(false);
    });
});