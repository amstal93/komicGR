import { injectable } from "inversify";

import IPostService from "../../Application.Services/Post/IPostService";

import Post from "../../Domain/Models/Post/Post";
import AddPostDTO from "../../Domain/Dtos/Post/AddPostDTO";
import EditPostDTO from "../../Domain/Dtos/Post/EditPostDTO";
import { createTestPost } from "../Helpers/postUtils";

@injectable()
export class PostServiceMOCK implements IPostService {
  getAllPosts(): Promise<Post[]> {
    throw new Error("Method not implemented.");
  }
  getPostByPostId(postId: number): Promise<Post> {
    return Promise.resolve(createTestPost(postId, 15));
  }
  getPostsByUserId(userId: number): Promise<Post[]> {
    throw new Error("Method not implemented.");
  }
  getPostsByCategoryId(
    categoryId: number,
    offset?: number,
    limit?: number
  ): Promise<Post[]> {
    throw new Error("Method not implemented.");
  }
  addPost(newPost: AddPostDTO): Promise<any> {
    throw new Error("Method not implemented.");
  }
  likePost(userId: number, postId: number, like: boolean): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  getNumberOfPostLikes(postId: number): Promise<number> {
    throw new Error("Method not implemented.");
  }
  deletePostById(userId: number, postId: number): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  editPost(postId: number, editPostDTO: EditPostDTO): Promise<{ id: number }> {
    return Promise.resolve({ id: 1 });
  }
  setPostAsUserFavourite(postId: number, userId: number): Promise<{ postId: number }> {
    return Promise.resolve({ postId: 1 });
  }
}
