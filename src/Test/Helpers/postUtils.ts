import Post from "../../Domain/Models/Post/Post";
import Tag from "../../Domain/Models/Tag/Tag";

const createdAt = new Date();

export function createTestPost(id?: number, userId?: number, tags?: Tag[], categoryId?: number) {
    return new Post(
        id || 1,
        "Test post title",
        "Test short body",
        "Test body",
        0,
        userId || 1,
        createdAt.toLocaleString(),
        "testImageurl",
        "/profile-images/26/profile.png",
        tags || [],
        categoryId || 1
    );
};

export function createTestPostWithNoUserId() {
    return new Post(
        1,
        "Test post title",
        "Test short body",
        "Test body",
        0,
        0,
        createdAt.toLocaleString(),
        "testImageurl",
        "/profile-images/26/profile.png"
    );
};

export function createTestPostWithNoTitle() {
    return new Post(
        2,
        "",
        "Test short body",
        "Test body",
        0,
        1,
        createdAt.toLocaleString(),
        "testImageurl",
        "/profile-images/26/profile.png"
    );
};

export function createTestPostWithNoBody() {
    return new Post(
        3,
        "Test post title",
        "Test short body",
        "",
        0,
        1,
        createdAt.toLocaleString(),
        "testImageurl",
        "/profile-images/26/profile.png"
    );
};

export function createTestPostOnlyWithTitleAndBody() {
    return new Post(
        4,
        "Test post title",
        "",
        "Test short body",
        0,
        1,
        createdAt.toLocaleString(),
        "testImageurl",
        "/profile-images/26/profile.png"
    );
};

export function createTestPostWithNoImageUrl() {
    return new Post(
        3,
        "Test post title",
        "Test short body",
        "Test body",
        0,
        1,
        createdAt.toLocaleString(),
        "",
        "/profile-images/26/profile.png"
    );
};

export function createTestPostWithNoUserProfileImageUrl() {
    return new Post(
        3,
        "Test post title",
        "Test short body",
        "Test body",
        0,
        1,
        createdAt.toLocaleString(),
        "testImageUrl",
        ""
    );
};