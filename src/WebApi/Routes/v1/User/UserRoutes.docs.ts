import { ProfileUserComponent } from "../../../../Domain/Dtos/User/ProfileUserResponseDTO";
import { RegisterUserComponent } from "../../../../Domain/Dtos/User/RegisterUserDTO";

const getAllUsers = {
    "/users/": {
        get: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Returns all the users",
            operationId: 'getUsers',
            responses: {
                200: {
                    description: "",
                    content: {
                        "application/json": {
                            schema: {
                                type: "array",
                                $ref: "#/components/ProfileUser"
                            }
                        }
                    }
                }
            }
        }
    }
}

const getUserProfile = {
    "/users/profile": {
        get: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Returns the user's profile",
            operationId: 'getUserProfile',
            responses: {
                200: {
                    description: "The user's profile",
                    content: {
                        "application/json": {
                            schema: {
                                $ref: "#/components/ProfileUser"
                            }
                        }
                    }
                }
            },
        }
    }
};

const confirmEmail = {
    "/users/confirmEmail/{token}": {
        get: {
            tags: ['Users'],
            description: "",
            operationId: "confirmEmail",
            responses: {
                302: {
                    description: "Redirect's to home page"
                },
                500: {
                    description: "Confirmation failed",
                    content: {
                        "text/html": {
                            schema: {
                                type: "string",
                                example: "<h1>Confirmation failed...</h1>"
                            }
                        }
                    }
                }
            },
            parameters: [{
                name: "token",
                in: "path",
                required: true,
                description: "A valid confirmation token",
                schema: {
                    type: "string",
                    example: "csdjSDdjvs98#EDndlD9"
                },
            }],
        }
    }
}

const getUserByPostId = {
    "/users/byPostId/{postId}": {
        get: {
            tags: ['Users'],
            description: "Returns the user that has the given id",
            operationId: 'getUserById',
            responses: {
                200: {
                    description: "The user",
                    content: {
                        "application/json": {
                            schema: {
                                $ref: "#/components/ProfileUser"
                            }
                        }
                    }
                }
            },
            parameters: [{
                name: "postId",
                in: "path",
                required: true,
                description: "The ID of the post",
                schema: {
                    type: "string",
                    example: "148"
                },
            }],
        }
    }
};

const getUserById = {
    "/users/{id}": {
        get: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Returns the user that has the given post id",
            operationId: 'getUserByPostId',
            responses: {
                200: {
                    description: "The user",
                    content: {
                        "application/json": {
                            schema: {
                                $ref: "#/components/ProfileUser"
                            }
                        }
                    }
                }
            },
            parameters: [{
                name: "id",
                in: "path",
                required: true,
                description: "The ID of the user",
                schema: {
                    type: "string",
                    example: "27"
                },
            }]
        }
    }
};

const register = {
    "/users/register": {
        post: {
            tags: ['Users'],
            description: "Register a new user",
            operationId: 'registerUser',
            consumes: [
                "application/json",
            ],
            responses: {
                201: {
                    description: "Register a new user",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                    msg: {
                                        type: "string",
                                        example: "User registered"
                                    },
                                    userId: {
                                        type: "number",
                                        example: parseInt('27')
                                    }
                                }
                            }
                        }
                    }
                }
            },
            requestBody: {
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#components/RegisterUser"
                        }
                    }
                }
            }
        }
    }
};

const authenticate = {
    "/users/authenticate": {
        post: {
            tags: ['Users'],
            description: "Authenticate a new user",
            operationId: 'authenticateUser',
            consumes: [
                "application/json",
            ],
            responses: {
                200: {
                    description: "Returns the token",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                    token: {
                                        type: "string",
                                        example: "sample token"
                                    },
                                }
                            }
                        }
                    }
                }
            },
            requestBody: {
                description: "The user's credentials",
                required: true,
                content: {
                    "application/json": {
                        schema: {
                            type: "object",
                            properties: {
                                username: {
                                    type: "string",
                                    example: "joe"
                                },
                                password: {
                                    type: "string",
                                    example: "password"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};

const uploadProfileImageUrl = {
    "/users/uploadProfileImageUrl": {
        post: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Upload a profile image",
            operationId: 'uploadProfileImageUrl',
            responses: {
                200: {
                    description: "Succesful operation",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                    msg: {
                                        type: "string",
                                        example: "Profile Image Url added succesfully"
                                    },
                                }
                            }
                        }
                    }
                }
            },
            requestBody: {
                description: "The image's url",
                required: true,
                content: {
                    "application/json": {
                        schema: {
                            type: "object",
                            properties: {
                                profileImageUrl: {
                                    type: "string",
                                    example: "profile_image.jpeg"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

const uploadImage = {
    "/users/uploadImage": {
        post: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Upload an image",
            operationId: 'uploadImage',
            responses: {
                200: {
                    description: "Succesful operation",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                    msg: {
                                        type: "string",
                                        example: "Profile Image added succesfully"
                                    },
                                }
                            }
                        }
                    }
                }
            },
            requestBody: {
                description: "The image to be uploaded",
                required: true,
                content: {
                    "multipart/form-data": {
                        schema: {
                            type: "object",
                            properties: {
                                userFile: {
                                    type: "string",
                                    format: "binary"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

const sendEmail = {
    "/users/sendEmail": {
        post: {
            tags: ['Users'],
            description: "Sends an email to a registered user to reset his/her password",
            operationId: 'sendEmail',
            consumes: [
                "application/json",
            ],
            responses: {
                200: {
                    description: "Succesful operation",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                }
                            }
                        }
                    }
                },
            },
            requestBody: {
                content: {
                    "application/json": {
                        schema: {
                            type: "object",
                            properties: {
                                email: {
                                    type: "stirng",
                                    example: "joe@doe.com"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

const resetPassword = {
    "/users/resetPassword": {
        post: {
            security: [{
                bearerAuth: []
            }],
            tags: ['Users'],
            description: "Reset a user's password",
            operationId: 'resetPassword',
            consumes: [
                "application/json",
            ],
            responses: {
                200: {
                    description: "Succesful operation",
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    success: {
                                        type: "boolean",
                                        example: true
                                    },
                                }
                            }
                        }
                    }
                },
                401: {
                    description: "Unauthorized operation",
                }
            },
            requestBody: {
                content: {
                    "application/json": {
                        schema: {
                            type: "object",
                            properties: {
                                newPassword: {
                                    type: "stirng",
                                    example: "newPassword123"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};


export const UsersPaths = {
    ...getAllUsers,
    ...getUserProfile,
    ...confirmEmail,
    ...getUserById,
    ...getUserByPostId,
    ...register,
    ...authenticate,
    ...uploadProfileImageUrl,
    ...uploadImage,
    ...sendEmail,
    ...resetPassword
}

export const UsersComponents = {
    ProfileUser: {
        ...ProfileUserComponent,
        properties: {
            success: {
                type: "boolean",
                example: true
            },
            user: ProfileUserComponent
        }
    },
    RegisterUser: RegisterUserComponent
}