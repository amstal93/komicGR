import passport from 'passport';
import { Request, Response } from 'express';
import {
  controller,
  httpPost,
  httpGet,
  httpDelete,
  httpPut
} from 'inversify-express-utils';
import { inject } from 'inversify';

import {
  handleAddPost,
  handleGetPostsByUserId,
  handleGetPostByPostId,
  handleSetLikeRequest,
  handleGetPostLikeStatusRequest,
  handleGetPostLikesRequest,
  handleGetPostsByCategoryId,
  handleDeletePostRequest,
  handleEditPost,
  handleSetPostAsUserFavourite
} from '../../Controllers/PostsController';
import { handleGetTags } from '../../Controllers/TagsController';
import TYPES from '../../../Helpers/DI/Types';
import IPostService from '../../../Application.Services/Post/IPostService';
import ITagService from '../../../Application.Services/Tag/ITagService';
import { handleError } from './utils';
import { ROUTES } from '../../../constants';

@controller(`/${ROUTES.API_VERSION.V1}/posts`)
export class PostRoute {
  constructor(
    @inject(TYPES.IPostService) private postService: IPostService,
    @inject(TYPES.ITagService) private tagService: ITagService
  ) {}

  // @httpGet('/')
  // async getAllPosts(req: Request, res: Response) {
  //     try {
  //         const response = await handleGetAllPosts(req, res, this.postService);

  //         res.send(response);
  //     } catch (err) {
  //         return handleError(err, res)
  //     }
  // }

  @httpGet('')
  async getPostByCategoryId(req: Request, res: Response) {
    try {
      const response = await handleGetPostsByCategoryId(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/tags')
  async getTags(req: Request, res: Response) {
    try {
      const response = await handleGetTags(req, res, this.tagService);

      res.send({ tags: response });
    } catch (err) {
      res.send({ errorMessage: 'Something went wrong when retrieving tags' });
    }
  }

  @httpGet('/like/:postId')
  async getPostLikeStatus(req: Request, res: Response) {
    try {
      const response = await handleGetPostLikeStatusRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/likes/:postId')
  async getPostLikes(req: Request, res: Response) {
    try {
      const response = await handleGetPostLikesRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/user/:userId', passport.authenticate('jwt', { session: false }))
  async getPostsByUserId(req: Request, res: Response) {
    try {
      const response = await handleGetPostsByUserId(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/:postId')
  async getPostById(req: Request, res: Response) {
    try {
      const response = await handleGetPostByPostId(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/like', passport.authenticate('jwt', { session: false }))
  async likePost(req: Request, res: Response) {
    try {
      const response = await handleSetLikeRequest(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost(
    '/user/favourites',
    passport.authenticate('jwt', { session: false })
  )
  async setPostAsUserFavourite(req: Request, res: Response) {
    try {
      const response = await handleSetPostAsUserFavourite(
        +req.body.postId,
        +req.userFromPassport.id,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPut('/:postId', passport.authenticate('jwt', { session: false }))
  async editPostWithId(req: Request, res: Response) {
    try {
      const postId = await handleEditPost(req, res, this.postService);

      res.send({ postId });
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/add', passport.authenticate('jwt', { session: false }))
  async addPost(req: Request, res: Response) {
    try {
      const response = await handleAddPost(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpDelete('/:postId', passport.authenticate('jwt', { session: false }))
  async deletePostById(req: Request, res: Response) {
    try {
      const response = await handleDeletePostRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }
}
