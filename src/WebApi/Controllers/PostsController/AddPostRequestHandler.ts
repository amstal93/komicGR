import AddPostDTO from "../../../Domain/Dtos/Post/AddPostDTO";
import IPostService from "../../../Application.Services/Post/IPostService";
import { Request, Response } from 'express';

export async function handleAddPost(req: Request, res: Response, postService: IPostService): Promise<{ success: boolean, postID?: number, msg: string }> {
    const newPost = <AddPostDTO>req.body;

    if(!newPost.user_id || !newPost.title || !newPost.body || !newPost.short_body|| !newPost.categoryId) {
        return { success: false, msg: "Required fields are missing"};
    }

    if(newPost.title.length < 3) {
        return { success: false, msg: "Title is too short. It should be at least 3 characters"}
    }
        
    const result = await postService.addPost(newPost);

    return { success: true, postID: result.insertId, msg: "Post added" };
}