import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

import PostResponseDTO from "../../../Domain/Dtos/Post/PostResponseDTO";
import Post from "../../../Domain/Models/Post/Post";

export async function handleGetPostByPostId(req: Request, res: Response, postService: IPostService): Promise<{ post: PostResponseDTO }> {
        const post = await postService.getPostByPostId(+req.params.postId);

        return { post: Post.toPostResponse(post) };
}