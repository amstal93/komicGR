import IPostService from "../../../Application.Services/Post/IPostService";
import { Request, Response } from "express";

export async function handleDeletePostRequest(
  req: Request,
  res: Response,
  postService: IPostService
): Promise<{ success: boolean; msg: string; postId?: number }> {
  const post = await postService.getPostByPostId(+req.params.postId);

  if (!Boolean(post)) {
    return { success: false, msg: "Post does not exist" };
  }

  if (post.user_id !== +req.userFromPassport.id) {
    return { success: false, msg: "You are not allowed to delete this post" };
  }

  const result = await postService.deletePostById(
    req.userFromPassport.id,
    +req.params.postId
  );

  if (result) {
    return { success: true, msg: "Post deleted", postId: +req.params.postId };
  }

  return { success: false, msg: "Something went wrong" };
}
