import { Request, Response } from "express";

import User from "../../../Domain/Models/User/User";
import IUserService from "../../../Application.Services/User/IUserService";
import ProfileUserResponseDTO from "../../../Domain/Dtos/User/ProfileUserResponseDTO";

export const handleGetAllUsersRequest = async (_: Request, __: Response, userService: IUserService): Promise<ProfileUserResponseDTO[]> => {
    const users = await userService.getAll();

    return User.toProfileResponseArray(users);
}