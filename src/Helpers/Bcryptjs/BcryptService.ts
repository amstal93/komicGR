import { genSalt, hash, compare } from 'bcryptjs';
import { injectable } from 'inversify';

@injectable()
class BcryptService {
    static async generateSalt(rounds?: number): Promise<string> {
        const salt = await genSalt(rounds);

        return salt;
    }

    static async createHashKey(value: string, salt: string): Promise<string> {
        const hashkey = await hash(value, salt);

        return hashkey;
    }

    static async comparePassword(candidatePassword: string, hashKey: string): Promise<Boolean> {
        const isMatch = await compare(candidatePassword, hashKey);

        return isMatch;
    }
}

export default BcryptService;