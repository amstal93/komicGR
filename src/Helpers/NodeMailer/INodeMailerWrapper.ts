import MailOptions from "../../Domain/Dtos/MailOptionsDTO";

interface INodeMailerWrapper {
    getTransporter(): any;
    sendEmail(mailOptions: MailOptions);
}

export default INodeMailerWrapper